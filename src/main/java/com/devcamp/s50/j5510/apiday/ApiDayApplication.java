package com.devcamp.s50.j5510.apiday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDayApplication.class, args);
	}

}
