package com.devcamp.s50.j5510.apiday;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CComboMenu {
    @CrossOrigin
	@GetMapping("/combomenu")
    public ArrayList<CMenu> getComBoMenu(){

        ArrayList<CMenu> menu = new ArrayList<>();
        CMenu sizeS = new CMenu('S', 20, 2, 200, 2, "150.000");
        CMenu sizeM = new CMenu('M', 25, 4, 300, 3, "200.000");
		CMenu sizeL = new CMenu('L', 30, 8, 500, 4, "250.000");    

        menu.add(sizeS);
		menu.add(sizeM);
		menu.add(sizeL);
		
		return menu;
        
    }
}
