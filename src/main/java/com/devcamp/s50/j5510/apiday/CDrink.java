package com.devcamp.s50.j5510.apiday;

public class CDrink {
    private String maNuocUong;
    private String tenNuocUong;
    private int dongia;
    private String ghiChu;
    private long ngayTao;
    private long ngayCapNhap;

    public CDrink(String maNuocUong , String tenNuocUong, int dongia, String ghiChu, long ngayTao,long ngayCapNhap ){
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.dongia = dongia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhap = ngayCapNhap;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public long getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public long getNgayCapNhap() {
        return ngayCapNhap;
    }

    public void setNgayCapNhap(long ngayCapNhap) {
        this.ngayCapNhap = ngayCapNhap;
    }

    @Override
    public String toString() {
        return "CDrink [dongia=" + dongia + ", ghiChu=" + ghiChu + ", maNuocUong=" + maNuocUong + ", ngayCapNhap="
                + ngayCapNhap + ", ngayTao=" + ngayTao + ", tenNuocUong=" + tenNuocUong + "]";
    }
    
}
