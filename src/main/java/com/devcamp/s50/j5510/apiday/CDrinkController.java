package com.devcamp.s50.j5510.apiday;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CDrinkController {
    @CrossOrigin
	@GetMapping("/DrinkMenu")
    public ArrayList<CDrink> getDrinkMenu(){
        ArrayList<CDrink> drink = new ArrayList<>();
        CDrink TraTac = new CDrink("TRA TAc", "Trà Tác", 10000,null,13072022, 13072022);
        CDrink CoCa = new CDrink("COCA", "Cocacola", 15000,null,13072022, 13072022);
        CDrink Monter = new CDrink("MONTER", "Monter", 15000,null,13072022, 13072022);
        CDrink Lavie = new CDrink("LAVIE", "Nước Lọc", 5000,null,13072022, 13072022);
        CDrink TraSua = new CDrink("TRA SUA", "Trà Sữa Trân Trâu", 40000,null,13072022, 13072022);
        CDrink BoHuc = new CDrink("RedBull", "Bò Húc", 15000,null,13072022, 13072022);
    
        drink.add(TraTac);
        drink.add(CoCa);
        drink.add(Monter);
        drink.add(Lavie);
        drink.add(TraSua);
        drink.add(BoHuc);
        
        return drink;
    }
}
