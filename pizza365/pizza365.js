$(document).ready(function(){
  onPageLoading();
  getComBoPizzaFromServer();
//Vùng 1: định nghĩa global variable (biến toàn cục)
  // bạn có thê dùng để lưu trữ combo được chọn, mỗi khi khách chọn bạn lại đổi giá trị properties của nó
  var gSelectedMenuStructure = {
              menuName: "",    // S, M, L
              duongKinhCM: 0,
              suongNuong: 0,
              saladGr: 0,
              drink: 0,
              priceVND: 0
  };
 // khai báo object order để chứa thông tin đặt hàng với 14 thuộc tính
 var gObjectRequest = {
              kichCo: "",
              duongKinh: "",
              suon: "",
              salad: "",
              loaiPizza: "",
              idVourcher: "",
              idLoaiNuocUong: "",
              soLuongNuoc: "",
              hoTen: "",
              thanhTien: "",
              email: "",
              soDienThoai: "",
              diaChi: "",
              loiNhan: ""
          };
  var gPercent = 0 ;



/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  $("#Btn-size0").on("click" , function(){
      onBtnSizeSClick();
  });
  $("#Btn-size1").on("click" , function(){
      onBtnSizeMClick();
  });
  $("#Btn-size2").on("click" , function(){
      onBtnSizeLClick();
  });
  $("#Btn-BACON").on("click" , function(){
      onBtnBACONClick();
  });
  $("#Btn-Haiwai").on("click" , function(){
      onBtnHawaiClick();
  });
  $("#Btn-OCEAN-MANIA").on("click" , function(){
      onBtnOCEANMANIAClick();
  });
  $("#btn-sendOrder").on("click" , function(){
      onBtnKiemTraDonClick();
  });
  $("#btn-Modal-sendOrder").on("click" , function(){
      onBtnCreatOrderClick();
  });


  

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // hàm xử lý sự kiện khi load trang
  function onPageLoading(){
    CallApiDrink();
    getMessager();
  }
  // hàm xử lý sự kiện click chọn size
  function onBtnSizeSClick(){
      "use strict"
       // map lựa chọn vào gSelectedMenuStructure
      gSelectedMenuStructure.menuName = "S"; 
      gSelectedMenuStructure.duongKinhCM = 20;
      gSelectedMenuStructure.suongNuong = 2;
      gSelectedMenuStructure.saladGr = 200;
      gSelectedMenuStructure.drink = 2;
      gSelectedMenuStructure.priceVND = 150000;

      // lưu vào biến toàn cục
      gObjectRequest.kichCo = "S";
      gObjectRequest.duongKinh = 20;
      gObjectRequest.suon = 2;
      gObjectRequest.salad = 200;
      gObjectRequest.soLuongNuoc = 2;
      gObjectRequest.thanhTien = 150000;
      // hiển thị thông tin ra console
      displayconsolog();
      // đổi màu button khi ấn vào 
      DoiMauKhiChonSizePizza("S");
      
  };
  // hàm xử lý sự kiện click chọn size
  function onBtnSizeMClick(){
      "use strict"
       // map lựa chọn vào gSelectedMenuStructure, chỗ này còn rất nhiều properties tôi chưa gán
      gSelectedMenuStructure.menuName = "M";
      gSelectedMenuStructure.duongKinhCM = 25;
      gSelectedMenuStructure.suongNuong = 4;
      gSelectedMenuStructure.saladGr = 300;
      gSelectedMenuStructure.drink = 3;
      gSelectedMenuStructure.priceVND = 200000;
      
      // lưu vào biến toàn cục
      gObjectRequest.kichCo = "M";
      gObjectRequest.duongKinh = 25;
      gObjectRequest.suon = 4;
      gObjectRequest.salad = 300;
      gObjectRequest.soLuongNuoc = 3;
      gObjectRequest.thanhTien = 200000;
      // hiển thị thông tin ra console
      displayconsolog();
      // đổi màu button khi ấn vào 
      DoiMauKhiChonSizePizza("M");
      
  };
  // hàm xử lý sự kiện click chọn size
  function onBtnSizeLClick(){
      "use strict"
      // map lựa chọn vào gSelectedMenuStructure  //chỗ này còn nhiều properties tôi chưa gán
      gSelectedMenuStructure.menuName = "L";
      gSelectedMenuStructure.duongKinhCM = 30;
      gSelectedMenuStructure.suongNuong = 6;
      gSelectedMenuStructure.saladGr = 400;
      gSelectedMenuStructure.drink = 4;
      gSelectedMenuStructure.priceVND = 250000;
      
      // lưu vào biến toàn cục
      gObjectRequest.kichCo = "L";
      gObjectRequest.duongKinh = 30;
      gObjectRequest.suon = 6;
      gObjectRequest.salad = 400;
      gObjectRequest.soLuongNuoc = 4;
      gObjectRequest.thanhTien = 250000;
      // hiển thị thông tin ra console
      displayconsolog();
      // đổi màu button khi ấn vào 
      DoiMauKhiChonSizePizza("L");
    
  };
  // hàm xử lý sự kiện khi click chọn loại pizzaa
  function onBtnOCEANMANIAClick(){
    "use strict"
    gObjectRequest.loaiPizza = "OCEAN MANIA"
    DoiMauKhiChonLoaiPizza("OCEAN MANIA");
    console.log("Loại Pizza Được Chọn Là :   " + gObjectRequest.loaiPizza)
  };
  // hàm xử lý sự kiện khi click chọn loại pizzaa
  function onBtnBACONClick(){
    "use strict"
    gObjectRequest.loaiPizza = "CHEESY CHICKEN BACON"
    DoiMauKhiChonLoaiPizza("CHEESY CHICKEN BACON");
    console.log("Loại Pizza Được Chọn Là :   " + gObjectRequest.loaiPizza)
  };
  // hàm xử lý sự kiện khi click chọn loại pizzaa
  function onBtnHawaiClick(){
    "use strict"
    gObjectRequest.loaiPizza = "HAWAIIAN"
    DoiMauKhiChonLoaiPizza("HAWAIIAN");
    console.log("Loại Pizza Được Chọn Là :   " + gObjectRequest.loaiPizza)
  };
  // hàm xử lý sự kiện khi click Gửi Đơn Hàng
  function onBtnKiemTraDonClick(){
    
    "use strict";
    //B1 Thu Thap Thông Tin
    readDataForm(gObjectRequest);
    //B2 Validate data
    var vCheck = validateDataForm(gObjectRequest);
    if(vCheck){
      $("#btn-sendOrder").removeAttr('class');
      $("#btn-sendOrder").addClass("btn btn-success w-100");
      displayconsolog();
      hienThiThongTinKhachHang();
      $("#SendOrder-Modal").modal("show")
      hienThiThongTinLenModalOrder();
      //B3 Gọi Api Gửi Đơn Hàng Lên Server
    }
    
  }
  // hàm xử lý khi xác nhận tạo đơn
  function onBtnCreatOrderClick(){
    onCreateOrderClick(gObjectRequest);
  }


  
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

  function DoiMauKhiChonSizePizza(paramSize){
    if(paramSize == "S"){
      $("#Btn-size0").removeAttr('class');
      $("#Btn-size1").removeAttr('class');
      $("#Btn-size2").removeAttr('class');
      $("#Btn-size0").addClass("btn btn-danger w-100");
      $("#Btn-size1").addClass("btn btn-warning w-100");
      $("#Btn-size2").addClass("btn btn-warning w-100");
    }
    else if(paramSize == "M"){
      $("#Btn-size0").removeAttr('class');
      $("#Btn-size1").removeAttr('class');
      $("#Btn-size2").removeAttr('class');
      $("#Btn-size0").addClass("btn btn-warning w-100");
      $("#Btn-size1").addClass("btn btn-danger w-100");
      $("#Btn-size2").addClass("btn btn-warning w-100");
    }
    else if(paramSize == "L"){
      $("#Btn-size0").removeAttr('class');
      $("#Btn-size1").removeAttr('class');
      $("#Btn-size2").removeAttr('class');
      $("#Btn-size0").addClass("btn btn-warning w-100");
      $("#Btn-size1").addClass("btn btn-warning w-100");
      $("#Btn-size2").addClass("btn btn-danger w-100");
    };
    // đay là ví dụ để bạn đẩy view đến 01 element khác (focus vào vùng đó)
    document.getElementById("about").scrollIntoView();
  }
    // hiển thị ra consolog chi  tiết combo pizza được chọn
    function displayconsolog(){
    console.log("%c Chi Tiết Combo Được Chọn Là : ", "color:pink");
    console.log("gói combo chọn là :   " +  gSelectedMenuStructure.menuName);
    console.log("Đường Kính là :   " +  gSelectedMenuStructure.duongKinhCM);
    console.log("Sườn Nướng là :   " +  gSelectedMenuStructure.suongNuong);
    console.log("Số Lượng Nước là :   " +  gSelectedMenuStructure.drink);
    console.log("Số Salad là :   " +  gSelectedMenuStructure.saladGr);
    console.log("Giá Tiên Là :   " +  gSelectedMenuStructure.priceVND);
    console.log("%c --------End------  ", "color:pink");
  }
  // Đổi màu button khi chọn loại pizza
  function DoiMauKhiChonLoaiPizza(paramPizza){
    if(paramPizza == "OCEAN MANIA"){
      $("#Btn-Haiwai").removeAttr('class');
      $("#Btn-BACON").removeAttr('class');
      $("#Btn-OCEAN-MANIA").removeAttr('class');
      $("#Btn-Haiwai").addClass("btn btn-warning w-100");
      $("#Btn-BACON").addClass("btn btn-warning w-100");
      $("#Btn-OCEAN-MANIA").addClass("btn btn-danger w-100");
    }
    else if(paramPizza == "CHEESY CHICKEN BACON"){
      $("#Btn-Haiwai").removeAttr('class');
      $("#Btn-BACON").removeAttr('class');
      $("#Btn-OCEAN-MANIA").removeAttr('class');
      $("#Btn-Haiwai").addClass("btn btn-warning w-100");
      $("#Btn-BACON").addClass("btn btn-danger w-100");
      $("#Btn-OCEAN-MANIA").addClass("btn btn-warning w-100");
    }
    else if(paramPizza == "HAWAIIAN"){
      $("#Btn-Haiwai").removeAttr('class');
      $("#Btn-BACON").removeAttr('class');
      $("#Btn-OCEAN-MANIA").removeAttr('class');
      $("#Btn-Haiwai").addClass("btn btn-danger w-100");
      $("#Btn-BACON").addClass("btn btn-warning w-100");
      $("#Btn-OCEAN-MANIA").addClass("btn btn-warning w-100");
    };
    // đay là ví dụ để bạn đẩy view đến 01 element khác (focus vào vùng đó)
    document.getElementById("order").scrollIntoView();
  }
  // hàm xử lý sự kiện load đồ uống vào form
  function CallApiDrink(){
      "use strict";
      $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
        type: "GET",
        async: true,
        dataType: "Json",
        success: function(dataRespontext){
          console.log(dataRespontext)
          loadDrinkToForm(dataRespontext);
        },
        error: function(error){
          alert(error.responseText)
        }
      })
  };
  // Tạo option và load đồ uống vào ô select đồ uống
  function loadDrinkToForm(paramDrinkObj){
    "use strict";
      $.each(paramDrinkObj,function(i, item){
          $("#Select-Drink").append($("<option>", {
              text: item.tenNuocUong,
              value: item.maNuocUong
          })).addClass("text-center font-weight-bold");

      })
  };
  // Thu Thập dữ Liệu Người dùng Trên Form
  function readDataForm(paramDataForm){
    paramDataForm.hoTen = $("#inp-fullname").val().trim();
    paramDataForm.email = $("#inp-email").val().trim();
    paramDataForm.soDienThoai = $("#inp-dien-thoai").val().trim();
    paramDataForm.diaChi = $("#inp-dia-chi").val().trim();
    paramDataForm.loiNhan = $("#inp-message").val().trim();
    paramDataForm.idVourcher = $("#inp-voucher-id").val().trim();

    paramDataForm.idLoaiNuocUong = $("#Select-Drink").val();
  };
  // validate tất cả dữ liệu đã nhập
  function validateDataForm(paramDataForm){
    "use strict";
    if(gSelectedMenuStructure.menuName == ""){
      alert("Bạn Chưa Chọn Size Pizza");
      return false;
    };
    if(paramDataForm.loaiPizza == ""){
      alert("Bạn Chưa Chọn Loại Pizza");
      return false;
    };
    if(paramDataForm.idLoaiNuocUong == ""){
      alert("Bạn Chưa Chọn Loại Đồ Uống");
      return false;
    };
    if(paramDataForm.hoTen == ""){
      alert("Bạn Chưa Nhập Họ Tên");
      return false;
    };
    if(paramDataForm.email == ""){
      alert("Bạn Chưa Nhập Địa Chỉ Email");
      return false;
    };
    if(paramDataForm.soDienThoai == "" || paramDataForm.soDienThoai.length < 8 || paramDataForm.soDienThoai.length > 11){
      alert("Bạn Chưa Nhập Đúng Số Điện Thoại");
      return false;
    };
    if(paramDataForm.diaChi == ""){
      alert("Bạn Chưa Nhập Địa Chỉ");
      return false;
    };
  
    if(!validateEmail(paramDataForm.email)){
          alert("Vui Lòng Nhập Đúng Email");
          return false;
    };
    if(paramDataForm.idVourcher == ""){
      gPercent = 0 ;
    }else{
      getVoucher(gObjectRequest.idVourcher);
    }
  
      return true;
  }
  // hàm kiểm tra email đúng chuẩn 
  function validateEmail(paramEmail){
      "use strict"
      const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return vREG.test(String(paramEmail).toLowerCase());
  };
  // hàm lấy dữ liệu voucher xem có hay không
  function getVoucher(paramDataVoucher){
    "use strict";
      $.ajax({
        url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramDataVoucher,
        type: "GET",
        async: false,
        dataType: "Json",
        success: function(dataRespontext){
          gPercent  = dataRespontext.phanTramGiamGia;    
          gObjectRequest.idVourcher = paramDataVoucher
          console.log("phần trăm giảm giá là : " + dataRespontext.phanTramGiamGia)
        },
        error: function(error){
           // không nhận lại được data do vấn đề gì đó: khả năng mã voucher ko dúng
          console.log("Không tìm thấy voucher " + error.responseText);
          gPercent = 0;
          gObjectRequest.idVourcher = "";
        }
      })
  };
   // hiển thị thông tin khách hàng 
   function hienThiThongTinKhachHang(){
    var vPrice = gSelectedMenuStructure.priceVND - (gSelectedMenuStructure.priceVND * gPercent) / 100;
    console.log("Loại Pizza Được Chọn Là:  " + gObjectRequest.loaiPizza);
    console.log("Loại Nước Uống Được Chọn là:  " + gObjectRequest.idLoaiNuocUong);
    console.log("Tên Khách Hàng Là:  " + gObjectRequest.hoTen);
    console.log("Địa Chỉ Là:  " + gObjectRequest.diaChi);
    console.log("Số Điện Thoại Là:  " + gObjectRequest.soDienThoai);
    console.log("Email là:  " + gObjectRequest.email);
    console.log("Lời Nhắn Là:  " + gObjectRequest.loiNhan);
    console.log("Đơn Giá Là:  " + gSelectedMenuStructure.priceVND + ".VND");
    console.log("phần trăm giảm giá là :   " + gPercent + "%");
    console.log("Tổng Số Tiền Phải Trả Là :   " + vPrice + ".VND");
  }
  // hiển thị thông tin khách hàng lên modal order
  function hienThiThongTinLenModalOrder(){
    var vPrice = gSelectedMenuStructure.priceVND - (gSelectedMenuStructure.priceVND * gPercent) / 100;
    $("#p-combo").html(gSelectedMenuStructure.menuName);
    $("#p-DuongKinh").html(gSelectedMenuStructure.duongKinhCM);
    $("#p-Suon").html(gSelectedMenuStructure.suongNuong);
    $("#p-SLN").html(gSelectedMenuStructure.drink);
    $("#p-Salar").html(gSelectedMenuStructure.saladGr);
    $("#p-LoaiPizza").html(gObjectRequest.loaiPizza);
    $("#p-LoaiNuocUong").html(gObjectRequest.idLoaiNuocUong);
    $("#p-GiaTien").html(gObjectRequest.thanhTien + " VNĐ");
    $("#p-Discount").html(gPercent + " %");
    $("#p-ThanhTien").html(vPrice + " VNĐ");
    $("#p-name").html(gObjectRequest.hoTen);
    $("#p-address").html(gObjectRequest.diaChi);
    $("#p-numberphone").html(gObjectRequest.soDienThoai);
    $("#p-email").html(gObjectRequest.email);
    $("#p-masseger").html(gObjectRequest.loiNhan);
  }
  //gọi tạo một order mới
  function onCreateOrderClick(paramObjData) {
    "use strict";
    var vInputSend = JSON.stringify(paramObjData);
      $.ajax({
        url:  "http://42.115.221.44:8080/devcamp-pizza365/orders",
        type: "POST",
        contentType:"application/json",
        data: vInputSend,
        success: function(dataRespontext){
          console.log(dataRespontext);
          $("#SendOrder-Modal").modal("hide");
          $("#result-orderid-modal").modal("show");
          $("#inp-result-orderID").val(dataRespontext.orderId);
        },
        error: function(error){
           alert("Chưa Tạo Được Đơn Hàng");
        }
      })
    };
    // gọi api lấy lời nhắn khuyến mại 
    function getMessager(){
      $.ajax({
        url: "http://localhost:8080/api/devcamp/message" ,
        type: "GET",
        success: function(response){
          console.log(response);
          LoiNhanKhuyenMai(response);
        }
      })
    }
    // gọi api lấy combo pizza
    function getComBoPizzaFromServer(){
      $.ajax({
        url: "http://localhost:8080/api/combomenu" ,
        type: "GET",
        async: false,
        dataType: "Json",
        success: function(response){
          console.log(response);
          showMenuCombo(response)
        }
      })
    }
    // lời nhắn khuyến mãi dựa vào thứ trong tuần
    function LoiNhanKhuyenMai(paramData){
      var bKhuyenMai = [
        {Date: "Thứ Hai" , Qua : "Mua Một Tặng Một Ngày Hôm Nay"},
        {Date: "Thứ Ba" , Qua : "Mua 2 Tặng Một Ngày Hôm Nay"},
        {Date: "Thứ Tư" , Qua : "Mua 3 Tặng Một Ngày Hôm Nay"},
        {Date: "Thứ Năm" , Qua : "Mua 4 Tặng Một Ngày Hôm Nay"},
        {Date: "Thứ Sáu" , Qua : "Mua 5 Tặng Một Ngày Hôm Nay"},
        {Date: "Thứ Bảy" , Qua : "Mua 6 Tặng Một Ngày Hôm Nay"},
        {Date: "Chủ Nhật" , Qua : "Mua 7 Tặng Một Ngày Hôm Nay"},
      ]
      for(var bI = 0 ; bI < bKhuyenMai.length ; bI++){
        if(bKhuyenMai[bI].Date == paramData){
          $("#p-message-api").html(bKhuyenMai[bI].Qua);
        }
      }

    }

    function showMenuCombo(paramCombo){
      for(var bI = 0; bI < paramCombo.length ; bI ++){
      var vShow = `
      <div class="col-sm-4">
                  <div class="card border border-warning">
                    <div class="card-header bg-dark text-white text-center anhnencombo">
                      <h3>`+ paramCombo[bI].size + `</h3>
                    </div>
                    <div class="card-body text-center">
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b> Đường Kính </b>`+ paramCombo[bI].duongKinh + `</li>
                        <li class="list-group-item"><b>sườn Nướng </b>`+ paramCombo[bI].suon + `</li>
                        <li class="list-group-item"><b> nước ngọt </b>`+ paramCombo[bI].soLuongNuocNgot + `</li>
                        <li class="list-group-item"><b> salad </b>`+ paramCombo[bI].salad + `g</li>
                        <li class="list-group-item">
                          <h1><b>`+ paramCombo[bI].donGia + `đ</b></h1> Giá Tiền
                        </li>
                      </ul>
                    </div>
                    <div class="card-footer text-center">
                      <button id="Btn-size`+ bI + `" class="btn btn-warning col-sm-12">Chọn</button>
                    </div>
                  </div>
                </div>`

              $("#setComboMenu").append(vShow);
            }
    }


})